<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new animal("shaun");

echo "nama animal : $sheep->name <br>"; // "shaun"
echo "legs count : $sheep->legs<br>"; // 4
echo "berdarah dingin? $sheep->cold_blooded <br><br>"; // "no"

$sungokong = new Ape("kera sakti");

echo "nama animal : $sungokong->name <br>"; 
echo "legs count : $sungokong->legs<br>"; 
echo "berdarah dingin? $sungokong->cold_blooded <br>"; // "no"
echo "Yell : "; // "Auooo"
echo $sungokong->yell(); // "Auooo"
echo "<br><br>"; // "Auooo"

$kodok = new Frog("buduk");

echo "nama animal : $kodok->name <br>"; 
echo "legs count : $kodok->legs<br>"; 
echo "berdarah dingin? $kodok->cold_blooded <br>"; 
echo "Jump : "; 
echo $kodok->jump(); // "Auooo"
echo "<br><br>"; // "Auooo"

