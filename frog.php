<?php

require_once('animal.php');

class Frog extends animal
{
    public $cold_blooded = "Yes";

    public function jump(){
        echo "Hop Hop";
    }

}